import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'

import Footer from '../Componentes/Footer';
import DashboardIcon from '@mui/icons-material/Dashboard';
import FlagIcon from '@mui/icons-material/Flag';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import SettingsIcon from '@mui/icons-material/Settings';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import CustomizedList from '../controladores/List';

const Inicio = () => {
    return (

        <body>
          
             
        <div class='col 3'>
            <a class="btn btn-primary" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
          <MenuIcon/>
            </a>
       

            <div class="offcanvas offcanvas-start bg-dark scrollbar-hidden " tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel" >
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasExampleLabel">    </h5>
                    <button type="button" class="btn-close " data-bs-dismiss="offcanvas" aria-label="Close" ></button>
                </div>
                <div class="offcanvas-body">
                    <div class="container  text-center" style={{ borderradius: 'red' }}>
                        <img src="img/logo.png" style={{ width: '200px', margin: '10px' }} alt="" />
                        <br/>
                
                    </div>
                    <div  class='text-start d-block ms-4'>
                        <CustomizedList name='Pagador'/>
                        <CustomizedList name='Proveedor'/>
                        <CustomizedList name='Asociar Planes'/>
                    </div>
                       <br></br>
                    <div class='text-start d-block ms-4'>
                        <a style={{color:'white'}}>< DashboardIcon /> Dashboard</a>
                        <br />
                        <a style={{color:'white'}}> <FlagIcon /> Notificaciones</a>
                        <br />
                        <a style={{color:'white'}}><CalendarMonthIcon /> Calendario</a>
                        <br />
                        <hr  style={{color:'white'}}/>
                        <a style={{color:'white'}}> <SettingsIcon /> Ajustes </a>
                        <br />
                        <a style={{color:'white'}}><LogoutIcon /> Salir </a>
                    </div>


                    <br /><br /><br /><br /><br /><br /><br />
                    <div class='d-block ms-4'>

                    <img src="img/logo3.png" style={{ width: "150px", marginleft: "20px" }} alt="" />
                    <li><a style={{ fontsize: "small", color: "aliceblue" }}>® Todos los derechos reservados</a></li>
               
                    </div>
                  </div>
            </div>
        </div>
        <div class='col 3'>

        </div>
     
 


<Footer />

</body>
    
      )
}

export default Inicio
