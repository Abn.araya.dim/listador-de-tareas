import React from 'react';
import {BrowserRouter as Router ,Route, Routes} from 'react-router-dom'
import './App.css';
import  Body from './Componentes/body'
import Footer from './Componentes/Footer';
import MainPage from './MainPAge/MainPage';
import Test from './controladores/Test';
import Inicio from './Inicio/Inicio';
function App() {
  return (
 
    <Router>
      <Routes>
        <Route exact path='/' Component={Body}/>
        <Route exact path='/login' Component={Footer}/>
        <Route exact path='/mainPage' Component={MainPage}/>
        <Route exact path='/test' Component={Test}/>
        <Route exact path='/Inicio' Component={Inicio}/>
      </Routes>

    </Router>

  
  );
}

export default App;
