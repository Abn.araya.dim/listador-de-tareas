import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import Footer from './Footer';
import Header from './header';
import FinanciamientoAutomotriz from './FinanciamientoAutomotriz';

import Infoleasin from './infoleasin';
import Login from './login';
import './login.css'

const Body = () => {
    return (
        <div>
            <body style={{ backgroundImage: "url(/IMG/fondo8.jpg)" }}>
                <Header />

                <div class="container-fluid">
                    <div class="row align-items-center ">
                        <div class='col-1'></div>
                        <div class="col-5" style={{ marginRight: "200px" }}>
                            <h2 class="" style={{ fontSize: "47px", color: "rgb(153, 199, 38)" }}>Financia tu negocio de forma ágil</h2>
                            <br />
                            <h4 style={{ fontSize: "30px", color: "rgb(255, 255, 255)" }}>Obtén liquidez inmediata anticipando el pago de tus cuentas por cobrar. Factoriza con las mejores condiciones del mercado y 100% digital.</h4>
                            <br />
                            <h5 style={{ color: "rgb(255, 255, 255)" }}>Conoce nuestras otras formas de financiamiento:</h5>
                            <br />
                            <div class="col-11">
                                <FinanciamientoAutomotriz />
                                <br />
                                <Infoleasin />
                            </div>
                        </div>
                        <div class="col-3" style={{ marginleft: "10px", marginTop: "20px" }}>
                            <Login />
                        </div>
                     <br/>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                 

                </div>
                <div className='test'>
                        <div class="bg"></div>
                        <div class="bg bg2"></div>
                        <div class="bg bg3"></div>
                    </div>
              
            </body>
        </div>
    )
}

export default Body;
